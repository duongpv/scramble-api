from app.models import *


def parse_basic_auth(request):
    if 'Authorization' not in request.headers:
        return None, None

    auth = request.headers.get('Authorization')[6:]
    return base64.b64decode(auth).split(':')[0], base64.b64decode(auth).split(':')[1]


def parse_token_auth(request):
    if 'Authorization' not in request.headers:
        return None

    return request.headers.get('Authorization')[6:]


def get_current_user(request):
    token = parse_token_auth(request)
    if not token:
        return None, None

    auth = Token.query.filter_by(token=token).first()
    if not auth:
        return None, None

    return auth, User.query.filter_by(userId=auth.userId).first()

