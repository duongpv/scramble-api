import hmac
import calendar
import base64

from passlib.hash import pbkdf2_sha256
from hashlib import sha1
from datetime import datetime


def hash(password):
    return pbkdf2_sha256.encrypt(password)


def hmac_sha1(username, password):
    h = hmac.new(key=str(calendar.timegm(datetime.utcnow().utctimetuple())), digestmod=sha1)
    h.update(username + password)

    return base64.b64encode(h.digest())


def verify(password, password_hash):
    return pbkdf2_sha256.verify(password, password_hash)
