import calendar
import json

from flask import request, Response, Blueprint

from app.models import *
from app.urls import *

flight_ctrl = Blueprint('flight_controller', __name__, url_prefix=FLIGHT_URL_PREFIX)


@flight_ctrl.route('/', methods=['POST', 'GET'])
def flight():
    if request.method == 'POST':
        try:
            data = request.get_json()
            if 'name' not in data:
                return Response(json.dumps({}), mimetype='application/json', status=400)

            if 'startdate' not in data:
                data['startdate'] = 0
            if 'enddate' not in data:
                data['enddate'] = 0
            if 'callsign' not in data:
                data['callsign'] = ''
            if 'notes' not in data:
                data['notes'] = ''

            flight = Flight(**data)
            db.session.add(flight)
            db.session.commit()

            response = {
                'data': {
                    'flightId': flight.flightId,
                    'name': flight.name
                },
                'status': 'CREATED'
            }

            return Response(json.dumps(response), mimetype='application/json', status=201)

        except:
            return Response(json.dumps({}), mimetype='application/json', status=400)

    elif request.method == 'GET':
        try:
            flights = Flight.query.order_by(Flight.lastupdated.desc()).all()
            data = []
            for flight in flights:
                f = {
                    'flightId': flight.flightId,
                    'name': flight.name,
                    'callsign': flight.callsign,
                    'notes': flight.notes,
                }

                if flight.startdate:
                    f['startdate'] = calendar.timegm(flight.startdate.timetuple())
                if flight.enddate:
                    f['enddate'] = calendar.timegm(flight.enddate.timetuple())
                if flight.lastupdated:
                    f['lastupdated'] = calendar.timegm(flight.lastupdated.timetuple())

                data.append(f)

            response = {
                'data': data,
                'status': ''
            }

            return Response(json.dumps(response), mimetype='application/json')

        except:
            return Response(json.dumps({}), mimetype='application/json', status=400)

    return Response(status=405)


@flight_ctrl.route('/<flight_id>', methods=['PUT', 'GET', 'DELETE'])
def get_flight(flight_id):
    try:
        flight = Flight.query.filter_by(flightId=flight_id).first()
        if not flight:
            response = {
                'data': None,
                'status': 'NOT_FOUND'
            }

            return Response(json.dumps(response), mimetype='application/json', status=404)

        if request.method == 'DELETE':
            db.session.delete(flight)
            db.session.commit()
            response = {
                'data': {
                    'flightId': flight_id,
                },
                'status': 'DELETED'
            }

            return Response(json.dumps(response), mimetype='application/json')

        positions = Position.query.filter(Position.flightId == flight_id).count()

        status = ''
        if request.method == 'PUT':
            data = request.get_json()
            if 'name' in data:
                flight.name = data['name']
            if 'notes' in data:
                flight.notes = data['notes']
            if 'callsign' in data:
                flight.callsign = data['callsign']
            if 'startdate' in data:
                flight.startdate = datetime.datetime.utcfromtimestamp(long(data['startdate']))
            if 'enddate' in data:
                flight.enddate = datetime.datetime.utcfromtimestamp(long(data['enddate']))

            flight.lastupdated = datetime.datetime.now()
            db.session.commit()

            status = 'UPDATED'

        f = {
            'flightId': flight.flightId,
            'name': flight.name,
            'callsign': flight.callsign,
            'notes': flight.notes,
            'positions': positions
        }

        if flight.startdate:
            f['startdate'] = calendar.timegm(flight.startdate.timetuple())
        if flight.enddate:
            f['enddate'] = calendar.timegm(flight.enddate.timetuple())
        if flight.lastupdated:
            f['lastupdated'] = calendar.timegm(flight.lastupdated.timetuple())

        response = {
            'data': f,
            'status': status
        }

        return Response(json.dumps(response), mimetype='application/json')

    except:
        return Response(json.dumps({}), mimetype='application/json', status=400)
