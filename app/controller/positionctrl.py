import calendar
import json

from flask import request, Response, Blueprint

from app.models import *
from app.urls import *

position_ctrl = Blueprint('position_controller', __name__, url_prefix=FLIGHT_URL_PREFIX)


@position_ctrl.route('/<flight_id>/position/', methods=['POST', 'GET'])
def position(flight_id):
    if request.method == 'POST':
        try:
            flight = Flight.query.filter_by(flightId=flight_id).first()
            if not flight:
                response = {
                    'data': None,
                    'status': 'NOT_FOUND'
                }

                return Response(json.dumps(response), mimetype='application/json', status=404)

            position = Position(flight=flight, **request.get_json())
            db.session.add(position)
            db.session.commit()

            response = {
                'data': {
                    'positionId': position.positionId,
                },
                'status': 'CREATED'
            }

            return Response(json.dumps(response), mimetype='application/json', status=201)

        except:
            return Response(json.dumps({}), mimetype='application/json', status=400)

    elif request.method == 'GET':
        try:
            positions = Position.query.filter_by(flightId=flight_id)
            data = []
            for position in positions:
                data.append({
                    'positionId': position.positionId,
                    'timestamp': calendar.timegm(position.timestamp.timetuple()),
                    'altitude': position.altitude,
                    'longitude': position.longitude,
                    'latitude': position.latitude,
                    'course': position.course,
                    'pressure': position.pressure,
                    'speed': position.speed,
                    'flightId': position.flightId
                })

            response = {
                'data': data,
                'status': ''
            }

            return Response(json.dumps(response), mimetype='application/json')

        except:
            return Response(json.dumps({}), mimetype='application/json', status=400)

    return Response(status=405)


@position_ctrl.route('/<flight_id>/position/<sequence>', methods=['GET'])
def get_position(flight_id, sequence):
    try:
        flight = Flight.query.filter_by(flightId=flight_id).first()
        if not flight:
            response = {
                'data': None,
                'status': 'NOT_FOUND'
            }

            return Response(json.dumps(response), mimetype='application/json', status=404)

        positions = Position.query.filter_by(flightId=flight_id).offset(sequence)
        if not positions:
            response = {
                'data': None,
                'status': 'NOT_FOUND'
            }

            return Response(json.dumps(response), mimetype='application/json', status=404)

        data = []
        for position in positions:
            data.append({
                'positionId': position.positionId,
                'timestamp': calendar.timegm(position.timestamp.timetuple()),
                'altitude': position.altitude,
                'longitude': position.longitude,
                'latitude': position.latitude,
                'course': position.course,
                'pressure': position.pressure,
                'speed': position.speed,
                'flightId': position.flightId
            })

        response = {
            'data': data,
            'status': ''
        }

        return Response(json.dumps(response), mimetype='application/json')

    except Exception as e:
        return Response(json.dumps({'e': e.message}), mimetype='application/json', status=400)
