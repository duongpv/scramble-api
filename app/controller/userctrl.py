import json
from sqlalchemy import or_, and_

from flask import request, Response, Blueprint

from app.urls import *
from app.util.authutil import *

user_ctrl = Blueprint('user_controller', __name__, url_prefix=USER_URL_PREFIX)


@user_ctrl.route('/', methods=['POST'])
def user():
    if request.method == 'POST':
        try:
            data = request.get_json()
            for field in ['username', 'password', 'email', 'tc']:
                if field not in data:
                    return Response(json.dumps({}), mimetype='application/json', status=401)

            if not data['tc']:
                return Response(json.dumps({}), mimetype='application/json', status=401)

            if db.session.query(User.userId).filter_by(username=data['username']).scalar() is not None:
                error = {
                    'error': 'Username \'%s\' already exists' % data['username']
                }
                return Response(json.dumps(error), mimetype='application/json', status=401)

            if db.session.query(User.userId).filter_by(email=data['email']).scalar() is not None:
                error = {
                    'error': 'Email \'%s\' already exists' % data['email']
                }
                return Response(json.dumps(error), mimetype='application/json', status=401)

            if 'userType' not in data:
                data['userType'] = 'Test'

            data.pop('tc', None)
            user = User(**data)
            db.session.add(user)
            db.session.commit()

            response = {
                'data': {
                    'userId': user.userId,
                    'username': user.username,
                    'email': user.email
                },
                'status': 'CREATED'
            }

            return Response(json.dumps(response), mimetype='application/json', status=201)

        except Exception as e:
            print e.message
            return Response(json.dumps({}), mimetype='application/json', status=400)

    return Response(status=405)


@user_ctrl.route('/login', methods=['POST'])
def login():
    if request.method == 'POST':
        try:
            username, password = parse_basic_auth(request)
            if not username or not password:
                return Response(status=401)

            user = User.query.filter_by(username=username).first()
            if not user:
                error = {
                    'error': 'User \'%s\' does not exist' % username
                }
                return Response(json.dumps(error), mimetype='application/json', status=401)

            if not verify(password, user.password):
                error = {
                    'error': 'Password incorrect'
                }
                return Response(json.dumps(error), mimetype='application/json', status=401)

            token = Token.query.filter_by(userId=user.userId).first()
            if token:
                response = {
                    'token': token.token
                }

                return Response(json.dumps(response), mimetype='application/json', status=201)

            token = Token(userid=user.userId,
                          username=user.username,
                          password=user.password)

            db.session.add(token)
            db.session.commit()

            response = {
                'token': token.token
            }

            return Response(json.dumps(response), mimetype='application/json', status=201)

        except:
            return Response(json.dumps({}), mimetype='application/json', status=400)

    return Response(status=405)


@user_ctrl.route('/logout', methods=['POST'])
def logout():
    auth, user = get_current_user(request)
    if user:
        db.session.delete(auth)
        db.session.commit()

        return Response(status=200)

    return Response(status=401)


@user_ctrl.route('/friend', methods=['POST', 'PUT', 'GET'])
def friend():
    auth, user = get_current_user(request)
    if not user:
        return Response(status=401)

    if request.method == 'POST':
        data = request.get_json()
        if 'friend' not in data:
            return Response(status=401)

        key = data['friend']
        friendUser = User.query.filter(or_(User.username == key, User.email == key)).first()
        if not friendUser:
            return Response(status=401)

        if friendUser.userId == user.userId:
            return Response(status=401)

        friend = Friend.query.filter(and_(Friend.userId == user.userId, Friend.friendId == friendUser.userId)).first()
        if friend:
            response = {
                'userId': friendUser.userId,
                'username': friendUser.username,
                'status': friend.status
            }

            return Response(json.dumps(response), mimetype='application/json', status=201)

        friend = Friend(userId=user.userId,
                        friendId=friendUser.userId,
                        status=0)

        db.session.add(friend)
        db.session.commit()

        response = {
            'userId': friendUser.userId,
            'username': friendUser.username,
            'status': 0
        }

        return Response(json.dumps(response), mimetype='application/json', status=201)

    elif request.method == 'PUT':
        data = request.get_json()
        if 'userId' not in data:
            return Response(status=401)

        if 'status' not in data:
            return Response(status=401)

        friend = Friend.query.filter(and_(Friend.userId == data['userId'], Friend.friendId == user.userId)).first()
        if not friend:
            return Response(status=401)

        friend.status = data['status']
        friend.updated = datetime.now()
        db.session.commit()

        return Response(status=201)

    elif request.method == 'GET':
        map = {
        }

        friends = Friend.query.filter_by(friendId=user.userId).all()
        for friend in friends:
            map[friend.userId] = friend.status

        data = []
        users = User.query.filter(User.userId.in_(map.keys())).all()
        for u in users:
            data.append({
                'userId': u.userId,
                'username': u.username,
                'status': map[u.userId]
            })

        response = {
            'data': data
        }

        return Response(json.dumps(response), mimetype='application/json', status=200)

    return Response(status=405)
