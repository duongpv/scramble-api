from flask import Flask
from flask_sqlalchemy import SQLAlchemy

from config import *
from util.hashutil import *

db = SQLAlchemy()


def create_app(name):
    app = Flask(name)
    db_url = 'postgresql://%s:%s@%s:%d/%s' % (DB_USERNAME, DB_PASSWORD, DB_HOST, DB_PORT, DB_NAME)
    app.config['SQLALCHEMY_DATABASE_URI'] = db_url
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    db.init_app(app)

    return app


class User(db.Model):
    __tablename__ = 'users'

    userId = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(128))
    password = db.Column(db.String(128))
    email = db.Column(db.String(128))
    userType = db.Column(db.String(128))
    created = db.Column(db.DateTime())

    def __init__(self, username, password, email, userType):
        self.username = username
        self.password = hash(password)
        self.email = email
        if userType in ['Individual', 'Corporate', 'Test']:
            self.userType = userType
        else:
            self.userType = 'Test'

        self.created = datetime.now()

    def __repr__(self):
        return '<User [%d] %s>' % (self.userId, self.username)


class Token(db.Model):
    __tablename__ = 'tokens'

    id = db.Column(db.Integer, primary_key=True)
    userId = db.Column(db.Integer)
    token = db.Column(db.String(256))
    created = db.Column(db.DateTime())

    def __init__(self, userid, username, password):
        self.userId = userid
        self.token = hmac_sha1(username, password)
        self.created = datetime.now()

    def __repr__(self):
        return self.token


class Friend(db.Model):
    __tablename__ = 'friends'

    id = db.Column(db.Integer, primary_key=True)
    userId = db.Column(db.Integer)
    friendId = db.Column(db.Integer)
    status = db.Column(db.Integer)
    created = db.Column(db.DateTime())
    updated = db.Column(db.DateTime())

    def __init__(self, userId, friendId, status):
        self.userId = userId
        self.friendId = friendId
        self.status = status
        self.created = datetime.now()
        self.updated = datetime.now()

    def __repr__(self):
        return '<Friend [%d] %s %s>' % (self.userId, self.friendId, self.status)


class Flight(db.Model):
    __tablename__ = 'flights'

    flightId = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128))
    startdate = db.Column(db.DateTime())
    enddate = db.Column(db.DateTime())
    callsign = db.Column(db.String(128))
    notes = db.Column(db.String(1024))
    lastupdated = db.Column(db.DateTime())

    def __init__(self, name, startdate, enddate, callsign, notes):
        self.name = name
        if startdate != 0:
            self.startdate = datetime.utcfromtimestamp(startdate)
        else:
            self.startdate = None
        if enddate != 0:
            self.enddate = datetime.utcfromtimestamp(enddate)
        else:
            self.enddate = None
        self.callsign = callsign
        self.notes = notes
        self.lastupdated = datetime.now()

    def __repr__(self):
        return '<Flight [%d] %s>' % (self.flightId, self.name)


class Position(db.Model):
    __tablename__ = 'positions'

    positionId = db.Column(db.Integer, primary_key=True)
    timestamp = db.Column(db.DateTime())
    altitude = db.Column(db.Float())
    longitude = db.Column(db.Float())
    latitude = db.Column(db.Float())
    course = db.Column(db.Float())
    pressure = db.Column(db.Float())
    speed = db.Column(db.Float())
    flightId = db.Column(db.Integer, db.ForeignKey('flights.flightId'))
    flight = db.relationship('Flight', foreign_keys=flightId)

    def __init__(self, altitude, longitude, latitude, course, pressure, speed, flight):
        self.timestamp = datetime.now()
        self.altitude = altitude
        self.longitude = longitude
        self.latitude = latitude
        self.course = course
        self.pressure = pressure
        self.speed = speed
        self.flight = flight

    def __repr__(self):
        return '<Position [%d]>' % self.positionId
