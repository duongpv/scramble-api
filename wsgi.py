from app.models import *
from app.controller import flightctrl, positionctrl, userctrl
from config import *

app = create_app(__name__)

app.register_blueprint(userctrl.user_ctrl)
app.register_blueprint(flightctrl.flight_ctrl)
app.register_blueprint(positionctrl.position_ctrl)


@app.before_first_request
def create_database():
    # for init db
    db.create_all()


if __name__ == '__main__':
    app.run(debug=DEBUG, host='0.0.0.0', port=52000)
