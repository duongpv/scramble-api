# for dev
nohup python wsgi.py > run.log &

# for production
gunicorn wsgi:app --bind 0.0.0.0:5000 --access-logfile run.log --error-logfile error.log --daemon